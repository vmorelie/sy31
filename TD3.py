import matplotlib.pyplot as plt
import numpy as np

xRepere = np.linspace(0,10,100)
x =[2.0,2.66,3.72,5.73,8.97,9.08]
y=[9.29,10.40,10.89,16.27,22.88,23.16]
i = 0
moyenneX = 0
moyenneY = 0
#calcul des moyennes
for k in x :
    moyenneX+=k
    moyenneY+=y[i]
    i+=1
moyenneX/=i
moyenneY/=i
#Calcul du y par la première méthode
sommeNum = 0
sommeDenom = 0
i=0
for k in x :
    sommeNum+=((k-moyenneX)*(y[i]-moyenneY))
    sommeDenom+=pow(k-moyenneX,2)
    i+=1
a = sommeNum/sommeDenom
b= moyenneY-a*moyenneX
yQuestion2 = a*xRepere+b
print("L'équation avec la minimisation des écarts non pondéré est de y ="+str(a)+" x + "+str(b))
#Question 4
#Initialisation
w=[1/6,1/6,1/6,1/6,1/6,1/6]
#itération ici 10 (Init rentre ici pour la minimisation de l'erreur)
for nbIte in range (0,10) :

    if nbIte != 0 :
        #mise à jour poids
        for i in range(0,6) :
            w[i]=1/abs(y[i]-a*x[i]-b)
        #normalisation poids
        somme = 0
        for i in range(0,6) :
            somme+=w[i]
        for i in range(0,6) :
            w[i]/=somme
    sommeNum = [0,[0,0]]
    sommeDenom =[0,0]
    for i in range(0,6) :
        sommeNum[0] += w[i]*x[i]*y[i]
        sommeNum[1][0] +=w[i]*x[i]
        sommeNum[1][1] +=w[i]*y[i]
        sommeDenom[0]+=w[i]*pow(x[i],2)

    a=(sommeNum[0]-sommeNum[1][0]*sommeNum[1][1])
    a/=sommeDenom[0]-pow(sommeNum[1][0],2)
    b=(sommeNum[1][1]-a*sommeNum[1][0])
    print("Itération : "+str(nbIte)+" a = "+str(a)+", b ="+str(b))
    yCourbe = a * xRepere + b
    plt.plot(xRepere, yCourbe, "--g")
plt.plot([2.02, 2.66, 3.72, 5.73,8.97,9.08], [9.29, 10.4, 10.89, 16.27,22.88,23.16], 'ro')
plt.plot(xRepere,yQuestion2,"-b")
plt.plot(xRepere,yCourbe,"-r")
plt.ylabel('Automatisation')
plt.grid()
plt.show()